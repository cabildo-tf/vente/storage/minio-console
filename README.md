# Minio

Object storage service, with Amazon S3 compatibility

# Install CLI

```bash
wget https://dl.min.io/client/mc/release/linux-amd64/mc
chmod +x mc
sudo mv mc /usr/local/bin
```

## Create alias
```bash
mc alias set minio https://vente-pre.tenerife.int WE1hyuGFfZhSK1kRA55H WPJlUFLezKW1QQ5jpZUAJjOPj8DpAhfGa3Xq8Qje
```

Test connection
```
mc admin info minio

●  vente-pre.tenerife.int
   Uptime: 29 minutes 
   Version: 2021-05-18T00:53:28Z
   Network: 1/1 OK 

0 B Used, 1 Bucket, 0 Objects

```
